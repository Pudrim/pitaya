module github.com/Pudrim/pitaya

go 1.12

require (
	github.com/DataDog/datadog-go v2.2.0+incompatible
	github.com/coreos/etcd v3.3.9+incompatible
	github.com/etcd-io/bbolt v1.3.1-coreos.6 // indirect
	github.com/etcd-io/etcd v3.3.9+incompatible // indirect
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.3.1
	github.com/google/uuid v1.0.0
	github.com/gorilla/websocket v1.2.0
	github.com/jhump/protoreflect v1.5.0
	github.com/nats-io/gnatsd v1.4.1
	github.com/nats-io/nats-server v1.4.1
	github.com/nats-io/nats.go v1.8.1
	github.com/opentracing/opentracing-go v1.0.2
	github.com/prometheus/client_golang v0.8.0
	github.com/sirupsen/logrus v1.0.6
	github.com/spf13/viper v1.0.2
	github.com/stretchr/testify v1.3.0
	github.com/topfreegames/go-workers v1.0.0
	github.com/topfreegames/pitaya v1.1.1
	github.com/uber/jaeger-client-go v2.13.0+incompatible
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
	google.golang.org/grpc v1.21.0
	gopkg.in/go-playground/validator.v9 v9.21.0
)
